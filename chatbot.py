from dotenv import load_dotenv
import openai
import inquirer
import os

load_dotenv()
openai.api_key  = os.getenv("OPENAI_API")

messages = []
def get_completion_from_messages(messages, model="gpt-3.5-turbo", temperature=0):
    questions = [
        inquirer.Text('message', message='User'),
    ]
    userMessage = inquirer.prompt(questions)
    messages.append({
        "role": "user",
        "content": userMessage["message"]
    })
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=temperature, # this is the degree of randomness of the model's output
    )
    messages.append(response.choices[0].message)
    print("Bot: ", response.choices[0].message.content)
    get_completion_from_messages(messages)

get_completion_from_messages(messages)