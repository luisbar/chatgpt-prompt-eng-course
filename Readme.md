## How to run it?
- This project was configure locally by using virtualenv
```bash
virtualenv --python=/usr/local/bin/python3 chatgpt-prompt-eng-course
```

- And the dependencies are listed in the `requirements.txt` file which was created by using
```bash
pip3 freeze > requirements.txt
```

- Run it by using
```bash
source ./bin/activate
```

- Deactivate the virtual env by using
```bash
deactivate
```

## Principle of promptings
### Principle 1: write clear and specific instructions
- Clear != short
- Tactics
  - Tactic 1: Use delimiters (check `principle1_tactic1.py`)
    - Triple quotes: "'"
    - Triple backticks: "
    - Triple dashes: ---,
    - Angle brackets: ‹ >,
    - XML tags: <tag› < /tag>
  - Tactic 2: Ask for structured output (check `principle1_tactic2.py`)
    - HTML
    - JSON
  - Tactic 3 (check `principle1_tactic3.py`)
    - Check whether conditions are satisfied
    - Check assumptions required to do the task
  - Tactic 4 (check `principle1_tactic4.py`)
    - Few-shot prompting
    - Give successful examples of completing tasks
    - Then ask model to perform the task

### Principle 2: give the model time to think
- Tactics
  - Tactic 1: specify the steps to complete a task (check `principle2_tactic1.py`)
  - Tactic 2: instruct the model to work out its own solution before rushing to a conclusion (check `principle2_tactic2.py`)

## Hints
- The key to being an effective prompt engineer isn't so much about knowing the perfect prompt, it's about having a good process to develop prompts that are effective for your application, to sum up, it is an iterative process
- You can summarize text by using ChatGPT
- You can infer by using ChatGPT, it means that you can analyze sentiments and get topics from a text
- You can transform a text
  - From a language to another language, e.g. from Spanish to English, in addition it can identify the language of a text and spell checking
  - From one format to another, e.g. from JSON to HTML
  - From a programming language to another programming language
- You can expand a text
  - Expanding is the task of taking a shorter piece of text, such as a set of instructions or a list of topics, and having the large language model generate a longer piece of text, such as an email or an essay about some topic
- At higher temperature the outputs from the model are kind of more random