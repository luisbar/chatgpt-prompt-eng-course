from dotenv import load_dotenv
import openai
import inquirer
import os

from principle1_tactic1 import prompt as prompt_1_principle_1_tactic_1
from principle1_tactic2 import prompt as prompt_1_principle_1_tactic_2
from principle1_tactic3 import prompt_1 as prompt_1_principle_1_tactic_3, prompt_2 as prompt_2_principle_1_tactic_3
from principle1_tactic4 import prompt as prompt_1_principle_1_tactic_4
from principle2_tactic1 import prompt_1 as prompt_1_principle_2_tactic_1, prompt_2 as prompt_2_principle_2_tactic_1
from principle2_tactic2 import prompt_1 as prompt_1_principle_2_tactic_2, prompt_2 as prompt_2_principle_2_tactic_2

questions = [
    inquirer.List(
        "exercise",
        message="What exercise would you like to execute?",
        choices=[
            "1. Prompt 1 Principle 1 Tactic 1",
            "2. Prompt 1 Principle 1 Tactic 2",
            "3. Prompt 1 Principle 1 Tactic 3",
            "4. Prompt 2 Principle 1 Tactic 3",
            "5. Prompt 1 Principle 1 Tactic 4",
            "6. Prompt 1 Principle 2 Tactic 1",
            "7. Prompt 2 Principle 2 Tactic 1",
            "8. Prompt 1 Principle 2 Tactic 2",
            "9. Prompt 2 Principle 2 Tactic 2",
        ],
    ),
]
exercises = {
    "1. Prompt 1 Principle 1 Tactic 1": prompt_1_principle_1_tactic_1,
    "2. Prompt 1 Principle 1 Tactic 2": prompt_1_principle_1_tactic_2,
    "3. Prompt 1 Principle 1 Tactic 3": prompt_1_principle_1_tactic_3,
    "4. Prompt 2 Principle 1 Tactic 3": prompt_2_principle_1_tactic_3,
    "5. Prompt 1 Principle 1 Tactic 4": prompt_1_principle_1_tactic_4,
    "6. Prompt 1 Principle 2 Tactic 1": prompt_1_principle_2_tactic_1,
    "7. Prompt 2 Principle 2 Tactic 1": prompt_2_principle_2_tactic_1,
    "8. Prompt 1 Principle 2 Tactic 2": prompt_1_principle_2_tactic_2,
    "9. Prompt 2 Principle 2 Tactic 2": prompt_2_principle_2_tactic_2
}

load_dotenv()
openai.api_key  = os.getenv("OPENAI_API")

def get_completion(prompt, model="gpt-3.5-turbo"):
    messages = [{"role": "user", "content": prompt}]
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=0, # this is the degree of randomness of the model's output
    )
    print(response.choices[0].message["content"])

def execute():
    answer = inquirer.prompt(questions)
    print("loading....")
    print(get_completion(exercises[answer["exercise"]]))
    execute()

execute()




